# HID ReMux

## Project Goals

### Primary
* ### Accept Input from USB devices
* ### Emulate one or more USB HID device(s) plugged into a host computer
* ### Selectively transform inputs into different output key(s) or macros

### Secondary
* ### Accept Input from a served web page
* ### Provide an intuitive web configuration interface
* ### Display served web page on built-in touch screen
* ### Emulate xbox controller

## Inputs

* USB
* GPIO & onboard inputs
* Web
* Touch screen


## Outputs

* USB as keyboard, mouse, and/or game controller
* Built-in screen
* Served web page

## Hardware

* **CPU:** ESP-32 or rPi Zero W
* **I/O interceptor:** Possibly on-CPU in software; otherwise sister micro ESP-32 or Arduino
* **Screen:** 4-7" capacitive touch IPS
* USB hub

## Software

* **Operating System:** OpenRTOS or Linux
* **Web Interface:** PHP, MariaDB/SQLite
* **Software key transcriber/macro engine:** PHP, pending latency testing.